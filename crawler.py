import requests
import sqlite3
from bs4 import BeautifulSoup
from urllib.parse import urlparse


class CrawlerSiteUrl :


    def create_connection(self) :
        self.connection = sqlite3.connect('test.db')
        cursor = self.connection.cursor()
        return cursor

    def is_valid(self, url):
        parsed = urlparse(url)
        return parsed.netloc and parsed.scheme

    def crawl(self) :
        cursor = self.create_connection()
        cursor.execute("CREATE TABLE IF NOT EXISTS source_table (url TEXT,id INTEGER AUTO_INCREMENT PRIMARY KEY);")
        cursor.execute("CREATE TABLE IF NOT EXISTS crawled_table (curl TEXT, cid INT, FOREIGN KEY (cid) REFERENCES source_table (id));")
        
        cursor.execute("INSERT INTO source_table(url) VALUES('https://www.zoomit.ir')")

        self.connection.commit()
        cursor.execute("SELECT url,id FROM source_table LIMIT 1")
        main_url = cursor.fetchone()
        req = requests.get(main_url[0])
        parsed_url = urlparse(main_url[0])
        soup = BeautifulSoup(req.content, 'html.parser')


        for link in soup.find_all("a"):
            href = link.get("href")
            if not href:
                continue
            is_valid_url = self.is_valid(href)
            if is_valid_url:
                url = href
            else:
                if href.startswith("/") or href.startswith("#"):
                    url = "{}://{}{}".format(parsed_url.scheme, parsed_url.netloc, href)
                else:
                    url = "{}://{}/{}".format(
                        parsed_url.scheme, parsed_url.netloc, href
                    )
            cursor.execute("INSERT INTO crawled_table(curl, cid) VALUES('{}','{}')".format(url, main_url[1]))
            
        self.connection.commit()
        cursor.close()


if __name__ == "__main__" :
    obj = CrawlerSiteUrl()
    obj.crawl()
